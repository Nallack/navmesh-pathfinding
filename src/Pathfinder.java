import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;

import javax.swing.*;

@SuppressWarnings("serial")
public class Pathfinder extends JFrame {
	
	private NavMesh navmesh;
	
	private JPanel canvas;
	private JPanel panelControls;
	private JButton buttonShowVisible;
	private JButton buttonShowMesh;
	private JButton buttonSolve;
	
	private Vector2 start;
	private Vector2 end;
	
	private Vector2[] path;
	
	private boolean showMesh = false;
	private boolean showVisible = false;
	private boolean showPath = false;
	private boolean showCoords = false;
	
	public Pathfinder()
	{
		super("Pathfinder");
		navmesh = new NavMesh();
		navmesh.setPoints(new Vector2[] {
				//border
				new Vector2(0,0), new Vector2(10,0), new Vector2(10, 10), new Vector2(0, 10),
				new Vector2(2,3), new Vector2(8,4), new Vector2(6, 7), new Vector2(3, 8)
		});
		navmesh.setFaces(new int[] {
				0, 1, 4,
				1, 5, 4,
				1, 2, 5,
				2, 6, 5,
				2, 6, 7,
				2, 3, 7,
				0, 4, 3,
				4, 7, 3
		});
		navmesh.setBoundaries(new int[] {
				//border
				0, 1, 1, 2, 2, 3, 3, 0,
				4, 5, 5, 6, 6, 7, 7, 4
		});
		navmesh.cacheVisible();
		
		start = new Vector2(1,1);
		end = new Vector2(9,9);
		
		initUI();
	}
	
	public void initUI()
	{
		JPanel panel = new JPanel(new GridBagLayout());
		{
			GridBagConstraints constraints = new GridBagConstraints();
			panelControls = new JPanel();
			{
				buttonShowVisible = new JButton("Visible");
				buttonShowVisible.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						showVisible = ! showVisible;
						repaint();
					}
				});
				panelControls.add(buttonShowVisible);
				
				buttonShowMesh = new JButton("Mesh");
				buttonShowMesh.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						showMesh = !showMesh;
						repaint();
					}
				});
				panelControls.add(buttonShowMesh);
				
				buttonSolve = new JButton("Solve");
				buttonSolve.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						solve();
					}
				});
				panelControls.add(buttonSolve);
			}
			panel.add(panelControls);
		}
		add(panel, BorderLayout.NORTH);
		
		canvas = new JPanel() {
			public void paint(Graphics g) {
				super.paint(g);
				draw((Graphics2D)g);
			}
		};
		canvas.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Point pos = e.getPoint();
				int modifiers = e.getModifiersEx();
				
				Dimension size = canvas.getSize();
				Rectangle2D box = getBoundingBox(navmesh.getPoints());
				
				double xoffset = box.getMinX();
				double yoffset = box.getMinY();
				double xfactor = size.getWidth() / box.getWidth();
				double yfactor = size.getHeight() / box.getHeight();
				
				if((modifiers & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK) {
					start.x = (float)(pos.x / xfactor + xoffset);
					start.y = (float)(pos.y / yfactor + yoffset);
				}
				if((modifiers & MouseEvent.BUTTON3_DOWN_MASK) == MouseEvent.BUTTON3_DOWN_MASK) {
					end.x = (float)(pos.x / xfactor + xoffset);
					end.y = (float)(pos.y / yfactor + yoffset);
				}
				solve();
			}
		});
		
		add(canvas);
		
		setSize(600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void solve() {
		//System.out.println("solving...");
		path = navmesh.findPath(start, end);
		//for(Vector2 point : path) System.out.println(point.toString());
		//System.out.println("Done");
		showPath = true;
		repaint();
	}
	
	private Rectangle2D getBoundingBox(Vector2[] points) {
		float minX = 0;
		float maxX = 1;
		float minY = 0;
		float maxY = 1;
		for(Vector2 point : points)
		{
			float x = (float)point.x;
			float y = (float)point.y;
			if(x <= minX) minX = x;
			if(x >= maxX) maxX = x;
			if(y <= minY) minY = y;
			if(y >= maxY) maxY = y;	
		}
		
		return new Rectangle2D.Float(minX - 1, minY - 1, maxX - minX + 2, maxY - minY + 2);
	}
	
	public void draw(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		graphics.drawString("size = [" + size.getWidth() + "," + size.getHeight() + "]", 20, 20);
		
		if(showMesh) drawFaces(graphics);
		if(showMesh) 
		drawPoints(graphics);
		drawBorders(graphics);
		if(showVisible) drawVisible(graphics);
		drawStartAndEnd(graphics);
		if(showPath) drawPath(graphics);
	}
	
	private void drawPoints(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		graphics.setColor(Color.BLACK);
		for(Vector2 point : navmesh.getPoints()) {
			int x = (int)((point.x - box.getMinX()) * xfactor);
			int y = (int)((point.y - box.getMinY()) * yfactor);
			graphics.fillOval(x - 3, y - 3, 6, 6);
			if(showCoords )graphics.drawString(point.toString(), x + 4, y);
		}
	}
	
	private void drawBorders(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		int[] boundaries = navmesh.getBoundaries();
		Vector2[] points = navmesh.getPoints();
		
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		graphics.setColor(Color.BLACK);
		graphics.setStroke(new BasicStroke(3));
		
		
		for(int i = 0; i < boundaries.length; i+=2) {
			int ax = (int)((points[boundaries[i]].x - box.getMinX()) * xfactor);
			int ay = (int)((points[boundaries[i]].y - box.getMinY()) * yfactor);
			int bx = (int)((points[boundaries[i + 1]].x - box.getMinX()) * xfactor);
			int by = (int)((points[boundaries[i + 1]].y - box.getMinY()) * yfactor);
			
			graphics.drawLine(ax, ay, bx, by);
		}
	}
	
	private void drawFaces(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		Vector2[] points = navmesh.getPoints();
		int[] faces = navmesh.getFaces();
		
		double xoffset = box.getMinX();
		double yoffset = box.getMinY();
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		for(int i = 0; i < faces.length; i+=3) {
			int[] xpoints = {
					(int)((points[faces[i]].x - xoffset) * xfactor), 
					(int)((points[faces[i + 1]].x - xoffset) * xfactor),
					(int)((points[faces[i + 2]].x - xoffset) * xfactor)
			};
			int[] ypoints = {
					(int)((points[faces[i]].y - yoffset) * yfactor), 
					(int)((points[faces[i + 1]].y - yoffset) * yfactor),
					(int)((points[faces[i + 2]].y - yoffset) * yfactor)
			};
			
			graphics.setColor(Color.CYAN);
			graphics.fillPolygon(new Polygon(xpoints , ypoints, 3));
			graphics.setColor(Color.YELLOW);
			graphics.drawLine(xpoints[0], ypoints[0], xpoints[1], ypoints[1]);
			graphics.drawLine(xpoints[1], ypoints[1], xpoints[2], ypoints[2]);
			graphics.drawLine(xpoints[2], ypoints[2], xpoints[0], ypoints[0]);
		}
	}
	
	private void drawStartAndEnd(Graphics2D graphics) {
		
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		int sx = (int)((start.x - box.getMinX()) * xfactor);
		int sy = (int)((start.y - box.getMinY()) * yfactor);
		int ex = (int)((end.x - box.getMinX()) * xfactor);
		int ey = (int)((end.y - box.getMinY()) * yfactor);
		
		graphics.setColor(Color.RED);
		graphics.fillOval((int)sx - 6, (int)sy - 6, 12, 12);
		if(showCoords) graphics.drawString(start.toString(), sx + 4, sy);
		graphics.setColor(Color.ORANGE);
		graphics.fillOval((int)ex - 6, (int)ey - 6, 12, 12);
		if(showCoords) graphics.drawString(end.toString(), ex + 4, ey);
	}
	
	private void drawVisible(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		Vector2[] points = navmesh.getPoints();
		boolean[][] visible = navmesh.getVisible();
		int count = points.length;
		
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		graphics.setColor(Color.MAGENTA);
		graphics.setStroke(new BasicStroke());
		for(int i = 0; i < count; i++) {
			for(int j = i + 1; j < count; j++) {
				if(visible[i][j]) {
					double xx = (points[i].x - box.getMinX()) * xfactor;
					double xy = (points[i].y - box.getMinY()) * yfactor;
					double yx = (points[j].x - box.getMinX()) * xfactor;
					double yy = (points[j].y - box.getMinY()) * yfactor;
					
					
					graphics.drawLine((int)xx, (int)xy, (int)yx, (int)yy);
				}
			}
		}
	}

	private void drawPath(Graphics2D graphics) {
		Dimension size = canvas.getSize();
		Rectangle2D box = getBoundingBox(navmesh.getPoints());
		
		double xfactor = size.getWidth() / box.getWidth();
		double yfactor = size.getHeight() / box.getHeight();
		
		graphics.setColor(Color.RED);
		graphics.setStroke(new BasicStroke(3));
		for(int i = 0; i < path.length - 1; i++) {
			
			int ax = (int)((path[i].x - box.getMinX()) * xfactor);
			int ay = (int)((path[i].y - box.getMinY()) * yfactor);
			int bx = (int)((path[i + 1].x - box.getMinX()) * xfactor);
			int by = (int)((path[i + 1].y - box.getMinY()) * yfactor);
			graphics.drawLine(ax, ay, bx, by);
		}
	}
}
