import javax.swing.*;

public final class Program {

	public static void main(String[] args) {
		
		//use system default look/feel
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//new LoginPanel().setVisible(true);
				new Pathfinder().setVisible(true);
			}
		});		
	}
}
