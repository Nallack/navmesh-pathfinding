import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class AlexCanvas extends JFrame {
	
	private CanvasPane canvas;
	private Graphics2D graphics;
	private Color backgroundColor;
	private Image canvasImage;
	
	public AlexCanvas(int width, int height) {
		canvas = new CanvasPane();
		
		setContentPane(canvas);
		setTitle("Canvas");
		setResizable(false);
		canvas.setPreferredSize(new Dimension(width, height));
		backgroundColor = Color.blue;
		pack();
		
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				System.out.println(e.getKeyChar());
			}
		});
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});	
	}
	
	public void setVisible(boolean visible) {
		if(graphics == null) {
			Dimension size = canvas.getSize();
			canvasImage = canvas.createImage(size.width, size.height);
			graphics = (Graphics2D)canvasImage.getGraphics();
			graphics.setColor(backgroundColor);
			graphics.fillRect(0, 0, size.width, size.height);
			graphics.setColor(Color.black);
		}
		super.setVisible(visible);
	}
	
	private class CanvasPane extends JPanel {
		public void paint(Graphics g) {
			g.drawImage(canvasImage, 0, 0, null);
		}
	}
}
