import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class LoginPanel extends JFrame {
	
	public static void main(String[] args) {
		new LoginPanel().setVisible(true);
	}
	
	private JLabel labelUsername = new JLabel("Enter username: ");
	private JLabel labelPassword = new JLabel("Enter password: ");
	private JTextField textUsername = new JTextField(20);
    private JPasswordField fieldPassword = new JPasswordField(20);
    private JButton buttonLogin = new JButton("Login");

	public LoginPanel() {
		super("Login");
		
		//Layout
		JPanel panel = new JPanel(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(10, 10, 10, 10);
		
		
		constraints.gridx = 0;
        constraints.gridy = 0; 
		panel.add(labelUsername, constraints);
		
		constraints.gridx = 1;
		panel.add(textUsername, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		panel.add(labelPassword, constraints);
		
		constraints.gridx = 1;
		panel.add(fieldPassword, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.CENTER;
		panel.add(buttonLogin, constraints);
		
		//Events
		buttonLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkLogin(textUsername.getText(), fieldPassword.getPassword());
			}
		});
		
		
		add(panel);
		pack();
		setLocationRelativeTo(null);
	}
	
	private void checkLogin(String username, char[] password)
	{
		char[] pw = {'a','d','m','i','n','\0'};
		if(username.equals("admin") && password.equals(pw))
		{
			System.out.println("Correct!");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Incorrect login information");
		}
	}
}
