/*
 * key value pair useful for priority queues
 */
public class Pair<K extends Comparable<K>, V> implements Comparable<Pair<K,V>>
{
	protected K key;
	protected V value;
	
	public Pair(K key, V value)
	{
		this.key = key;
		this.value = value;
	}
	
	public K getKey() { return key; }
	public V getValue() {return value; }
	
	public int compareTo(Pair<K,V> otherPair)
	{
		return key.compareTo(otherPair.key);
	}
}