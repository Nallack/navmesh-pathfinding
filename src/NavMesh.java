import java.util.*;

public class NavMesh {
	
	private Vector2[] points;
	private int[] faces;
	private int[] boundaries;
	private boolean[][] visible;
	
	public NavMesh() {
		points = new Vector2[0];
		faces = new int[0];
		boundaries = new int[0];
		visible = new boolean[0][0];
	}
	
	public Vector2[] getPoints() { return points; }
	public void setPoints(Vector2[] points) { this.points = points; }
	
	public int[] getFaces() { return faces; }
	public void setFaces(int[] faces) { this.faces = faces; }
	
	public int[] getBoundaries() { return boundaries; }
	public void setBoundaries(int[] boundaries) { this.boundaries = boundaries; }
	
	public boolean[][] getVisible() { return visible; }
	
	public boolean linecastBorders(Vector2 a, Vector2 b)
	{		
		//System.out.println(a + "->" + b);
		
		Vector2 c;
		Vector2 d;
		float denominator;
		for(int i = 0; i < boundaries.length; i+=2)
		{
			c = points[boundaries[i]];
			d = points[boundaries[i + 1]];
			
			//6 + 3 + 8 = 17! operations
			Vector2 atob = b.subtract(a);
			Vector2 atoc = c.subtract(a);
			Vector2 ctod = d.subtract(c);
			
			if(0 == (denominator = atob.x * ctod.y - atob.y * ctod.x)) continue;
			
			float u = (atob.y * atoc.x - atob.x * atoc.y) / denominator;
			float v = (ctod.y * atoc.x - ctod.x * atoc.y) / denominator;
						
			//30 operations
			//float u = -((a.y - b.y) * (c.x - a.x) + (b.x - a.x) * (c.y - a.y)) / ((a.y - b.y) * (d.x - c.x) + (b.x - a.x) * (d.y - c.y));
			//float v = -((c.y - d.y) * (a.x - c.x) + (d.x - c.x) * (a.y - c.y)) / ((c.y - d.y) * (b.x - a.x) + (d.x - c.x) * (b.y - a.y));
			
			if(0.00001 < u && u < 0.99999 && 0.00001 < v && v < 0.99999) {
				//System.out.println("blocked, c:" + c + " d" + d + " u:" + u + " v:" + v);
				return false;
			}
		}
		return true;
	}
	
	public boolean linecastFaces(Vector2 a, Vector2 b) {
		Vector2 c;
		Vector2 d;
		float denominator;
		for(int i = 0; i < faces.length; i+=3)
		{
			for(int j = 0; j < 3; j++) {
				
				c = points[faces[i + j]];
				d = points[faces[i + (j + 1) % 3]];
				
				//6 + 3 + 8 = 17! operations
				Vector2 atob = b.subtract(a);
				Vector2 atoc = c.subtract(a);
				Vector2 ctod = d.subtract(c);
				
				//check parallel
				if(0 == (denominator = atob.x * ctod.y - atob.y * ctod.x)) {
					float un = (atob.y * atoc.x - atob.x * atoc.y);
					float vn = (ctod.y * atoc.x - ctod.x * atoc.y);
					if(un == 0 || vn == 0) return false;
					else continue;
				};
				
				float u = (atob.y * atoc.x - atob.x * atoc.y) / denominator;
				float v = (ctod.y * atoc.x - ctod.x * atoc.y) / denominator;
							
				if(0 < u && u < 1 && 0 < v && v < 1) {
					return false;
				}
			}
		}
		return true;
	}
	
	//just convert cartesian to barycentric
	public boolean pointcastFaces(Vector2 p) {
		for(int i = 0; i < faces.length; i+=3) {
			Vector2 a = points[faces[i]];
			Vector2 b = points[faces[i + 1]];
			Vector2 c = points[faces[i + 2]];
			
			Vector2 ctop = p.subtract(c);
			
			float det = (b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y);
			float s = ((b.y - c.y) * ctop.x + (c.x - b.x) * ctop.y) / det;
			float t = ((c.y - a.y) * ctop.x + (a.x - c.x) * ctop.y) / det;
			
			//float det = (b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y);
			//float s = ((b.y - c.y) * (p.x - c.x) + (c.x - b.x) * (p.y - c.y)) / det;
			//float t = ((c.y - a.y) * (p.x - c.x) + (a.x - c.x) * (p.y - c.y)) / det;
			float u = 1 - s - t;
			
			if(0 < s && s < 1 && 0 < t && t < 1 && 0 < u && u < 1) {
				return true;
			}
		}
		return false;
	}
	
	public Vector2[] findPath(Vector2 start, Vector2 end)
	{
		int count = points.length;
		
		if(!pointcastFaces(start)) {
			System.out.println("start is not in mesh");
			return new Vector2[0];
		}
		if(!pointcastFaces(end)) {
			System.out.println("end is not in mesh");
			return new Vector2[0];
		}
		
		//case 1: points are visible
		if(linecastBorders(start, end)) {
			return new Vector2[] { start, end };
		}
			
		//case 2: need to pass around walls
		List<Vector2> path = new ArrayList<Vector2>();
		PriorityQueue<Pair<Float, Integer>> queue = new PriorityQueue<Pair<Float, Integer>>();
		Map<Vector2, Vector2> parents = new HashMap<Vector2, Vector2>(count);		
		boolean found = false;
		
		//put all visible vertices in a priority queue by a* distance
		for(int i = 0; i < count; i++) {
			if(linecastBorders(start, points[i])) {
				float dist = start.subtract(points[i]).magnitude()
								+ end.subtract(points[i]).magnitude();
				
				queue.add(new Pair<Float, Integer>(dist, i));
				parents.put(points[i], start);
			}
		}
		
		//search until a vertex can see the end
		while(!queue.isEmpty())
		{
			Pair<Float, Integer> node = queue.poll();
			float dist = node.getKey();
			int index = node.getValue();
			Vector2 point = points[index];
			
			if(linecastBorders(point, end)) {
				parents.put(end, point);
				found = true;
				break;
			}
			
			for(int i = 0; i < count; i++) {
				if(visible[index][i] && !parents.containsKey(points[i])) {
						float nextDist = dist + point.subtract(points[i]).magnitude();
						queue.add(new Pair<Float, Integer>(nextDist, i));
						parents.put(points[i], point);
				}
			}
		}
		
		if(found) {
			path.add(end);
			Vector2 v = end;
			while(v != null && !v.equals(start)) {
				v = parents.get(v);
				path.add(v);
			}
			Collections.reverse(path);
		}
		
		return path.toArray(new Vector2[0]);
	}
	
	public void generateBoundaries() {
	}
	
	public void cacheVisible()
	{
		//paths between barriers and outside of mesh are being calculated
		//to solve this, we say it is visible only if both:
		//1: raycast does not cross a barrier, (can travel along)
		//2: raycast intersects at least one triangle edge (not at ends)
		
		int count = points.length;
		visible = new boolean[count][count];
		for(int i = 0; i < count; i++)
		{
			for(int j = i + 1; j < count; j++)
			{
				//System.out.println(i + " -> " + j);
				visible[i][j] = false;
				if(linecastBorders(points[i], points[j])) {
					//System.out.println("no borders");
					if(!linecastFaces(points[i], points[j])) {
						//System.out.println("a face found!");
						visible[i][j] = true;
					} else {
						//System.out.println("no face found!");
					}
				}
				visible[j][i] = visible[i][j];
			}
		}
	}

}






