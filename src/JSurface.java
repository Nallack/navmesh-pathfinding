import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


@SuppressWarnings("serial")
public class JSurface extends JPanel {
	
	public JSurface(int width, int height) {
		setSize(new Dimension(width, height));
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		draw((Graphics2D)g);
	}
	
	
	protected void draw(Graphics2D g) {
		//g.drawString("java!", 20, 20);
	}
}
