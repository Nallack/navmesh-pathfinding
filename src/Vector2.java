import java.lang.Math;

public class Vector2 {
	static final Vector2 ZERO = new Vector2(0f,0f);
	
	public float x;
	public float y;
	
	public Vector2() {
	}
	
	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2 add(Vector2 v) {
		return new Vector2(x + v.x, y + v.y);
	}
	
	public Vector2 subtract(Vector2 v) {
		return new Vector2(x - v.x, y - v.y);
	}
	
	public Vector2 divide(Vector2 v) {
		return new Vector2(x / v.x, y / v.y);
	}
	
	public float magnitude() {
		return (float)Math.sqrt(x * x + y * y);
	}
	
	@Override
	public String toString() {
		return "x:" + x + " y:" + y;
	}
}
